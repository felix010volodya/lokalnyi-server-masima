<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegisterObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            ->add('type_object',ChoiceType::class,array(
                'choices' => array(
                    'Коттедж' => 'Коттедж',
                    'Пансионат' => 'Пансионат'
                )
            ))
            ->add('NumberOfRooms')
            ->add('name')
            ->add('ContactPerson')
            ->add('adress')
            ->add('price')

            ->add('Далее', SubmitType::class);

    }
}
